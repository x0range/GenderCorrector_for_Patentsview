#!/usr/bin/python
"""
Script for improving the USPTO patentsview inventor gender inference from 
inventor names. We use the name table by Philippe Remy to perform our own 
inference and only keep the existing gender inference if our inference 
is inconclusive.

Data sources:
    USPTO patentsview inventor demographics
        https://s3.amazonaws.com/data.patentsview.org/download/
                                g_inventor_disambiguated.tsv.zip
    Philippe Remy name list
        https://github.com/philipperemy/name-dataset/blob/master/
                                names_dataset/v3/first_names.zip
    
Results:
    M -> F reclassification:  1.0 %
    F -> M reclassification:  2.3 %
    NA -> M reclassification: 5.8 %
    NA -> F reclassification: 0.4 %
    Old shares: 
        M:   77.6 %
        F:   10.0 %
        NA:  12.4 %
    Combined shares: 
        M:   84.7 %
        F:    9.2 %
        NA:   6.1 %
"""

import pandas as pd

def gender_check(firstname):
    """ Function for infering gender from first names. The function 
            separates first names at spaces and considers each part 
            separately. In case of double names with hyphen, it 
            considers both the double name and the names individually.
            The assessment is made on the basis of the name list by
            Philippe Remy https://github.com/philipperemy/name-dataset/
            blob/master/names_dataset/v3/first_names.zip.
        @param firstname (str): First name or list of first names as str.
        @return gender: Infered gender. Either "M" or "F" or None.
    """ 
    
    """ Only check valid first names"""
    if isinstance(firstname, str):
        """ separate different names"""
        fns = firstname.split(" ")
        fn_additional = []
        for fn in fns:
            if "-" in fn:
                fn_additional += fn.split("-")
        fns += fn_additional
        
        """ check gender for each"""
        gender = None
        for fn in fns:
            fn_gender = name_df_reduced.get(fn)
            if fn_gender is not None:
                if gender is None:
                    gender = fn_gender
                elif fn_gender != gender:
                    gender = "Both"
        if gender == "Both":
            gender = None
    else:
        gender = None
    return gender


""" Read name list by Philippe Remy from https://github.com/
 philipperemy/name-dataset/blob/master/names_dataset/v3/first_names.zip.
    It should be downloaded and unzipped (will provide a json file)
    and placed in the wd.
"""
name_df = pd.read_json("first_names.json")
name_df = name_df.T

""" Identify multi-part names for filtering. We want to consider all 
    strings separately, so multi-part names are not useful."""
name_df["invalid_name"] = name_df.index.str.contains(' ', na=True, regex=False)

""" Create gender likelihood columns"""
name_df["gender_F"] = name_df["gender"].apply(lambda x: x.get("F"))
name_df["gender_M"] = name_df["gender"].apply(lambda x: x.get("M"))

""" Transform to binary; we consider 93% for either male or female to 
    denotes a traditionally male/female name."""
name_df.loc[name_df["gender_M"]>0.93,"likely_gender"] = "M"
name_df.loc[name_df["gender_F"]>0.93,"likely_gender"] = "F"

""" Reduce data frame to a simple lookup table"""
name_df_reduced = name_df.loc[(name_df["invalid_name"]==False)\
                  &((name_df["likely_gender"]=="M")\
                  |(name_df["likely_gender"]=="F")), "likely_gender"]

""" Load inventor data"""
inventor_df = pd.read_csv("g_inventor_disambiguated.tsv", sep="\t")

""" Perform the gender inference and transform to binary analogous to 
    column 'male_flag'"""
inventor_df["infered_gender"] = \
        inventor_df["disambig_inventor_name_first"].apply(gender_check)
inventor_df.loc[inventor_df["infered_gender"]=="M","infered_male_flag"] = 1.0
inventor_df.loc[inventor_df["infered_gender"]=="F","infered_male_flag"] = 0.0

""" Diagnostics: Print joint frequencies of existing and infered male flag"""
print(inventor_df.value_counts(["male_flag", "infered_male_flag"], 
                               dropna=False, normalize=True))

""" Replace old flag with a combined version (we keep the old one only if the
    inference is inconclusive."""
inventor_df["male_flag_old"] = inventor_df["male_flag"]
inventor_df.loc[~inventor_df["infered_male_flag"].isna(), "male_flag"] = \
                inventor_df.loc[~inventor_df["infered_male_flag"].isna(), 
                "infered_male_flag"]
del inventor_df['infered_gender']
del inventor_df['infered_male_flag']


""" Diagnostics: Print joint frequencies and frequencies of existing and 
    combined male flag"""
print(inventor_df.value_counts(["male_flag", "male_flag_old"], dropna=False, 
                               normalize=True))
print(inventor_df.value_counts("male_flag_old", dropna=False, normalize=True))
print(inventor_df.value_counts("male_flag", dropna=False, normalize=True))

""" Save new data frame"""
inventor_df.to_csv(sep="\t")
